# Parallel Programming in Python: Multithreading and Multiprocessing

Material for the threading and multiprocessing in python workshop.

## Objectives
Learning Outcomes for attendees of this workshop:

* Able to understand the differences between multithreading and multiprocessing.
* Know which tasks are better suited for multithreading or multiprocessing
* Able to use threading and multiprocessing packages for appropriately suited tasks.

## Outline
* Overview:
  * [Overview of parallel computing with python](notebooks/overview.ipynb)
* [Threading:](notebooks/threading.ipynb)
  * c-extension thread enabled libraries (numpy & scipy)
  * threading package
* [Multiprocessing:](notebooks/multiproc.ipynb)
  * Process class
  * Pool class
* Other parallel libraries:
  * [pymp -- OpenMP-like functionality for Python](notebooks/pymp.ipynb)


