from mpi4py import MPI 
comm = MPI.COMM_WORLD 
rank = comm.Get_rank() 
#
# Run on 6 proc.
# Assign the integer variable myint = 0 for rank 0, 1 for rank 1.
# Check by writing that assignment is correct
# Use MPI_BCAST to broadcast the variable of myint from rank 0
# Have each rank WRITE the value of myint. Check that every rank writes 0.
#=====

#=====
